import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core'

@NgModule({
 imports:      [ CommonModule ],

}) 

export class ProtectedModule { }


export class DestinoViaje {

	private selected: boolean;
	public servicios: string[];

	constructor(public nombre:string, public u:string, public votes: number = 0) {

		this.servicios = ["piscina", 'desayuno'];
	 }
		
	setSelected(s: boolean){
		this.selected = s;
	}

	isSelected(): boolean{
		return this.selected;
	}

	 voteUp(): any {
       this.votes++;
   }
   	voteDown(): any {
       this.votes--;
   }
}
