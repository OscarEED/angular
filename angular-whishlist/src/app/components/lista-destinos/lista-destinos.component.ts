import { Component,EventEmitter, Output, OnInit  } from '@angular/core';
import { DestinoViaje } from './../../model/destino-viaje.model';
import { DestinosApiClient } from './../../model/destinos-api-client.model';
import { CommonModule } from '@angular/common';


import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';



import { DestinosViajesState } from './../../model/destinos-viajes-state.model';
 
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
  })


export class ListaDestinosComponent implements OnInit {

	@Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
  	
    this.onItemAdded = new EventEmitter;
    this.updates = [];

    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        const d = data;
        if (d != null) {
          this.updates.push('Se eligió: ' + d.nombre);
        }
      });

    store.select(state => state.destinos.items).subscribe(items => this.all = items);

   }

  ngOnInit(): void {

     this.store.select(state => state.destinos)
    .subscribe(data => {
      let d = data.favorito;
      if (d != null) {
        this.updates.push("Se eligió: " + d.nombre);
      }
    });

  }
  	agregado(d: DestinoViaje) {

  		this.destinosApiClient.add(d);
  		this.onItemAdded.emit(d);
      
  	}

  	elegido(e: DestinoViaje) {
      
  	   this.destinosApiClient.elegir(e);
    
    }

     getAll(){

    }
}
